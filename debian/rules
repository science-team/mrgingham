#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

# Needed for cross-building:
#
#   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=853881
#
# According to the report, debhelper 10 supports this already, but I'd like to
# be compatible with older distros for now, so I still set this manually
export PKG_CONFIG := $(DEB_HOST_GNU_TYPE)-pkg-config
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

%:
	dh $@ --with python3

# I build everything, including the default python libraries
#
# And then I build the python libraries for all the available python versions,
# which could be more than just the default. The default one won't be rebuilt,
# because the build system will see it's done already
override_dh_auto_build:
	export VERSION=$(shell echo $(DEB_VERSION) | sed "s/-.*//");	\
	for v in `py3versions -s | sed s/python//g`; do			\
	  rm -f *pywrap*.o;						\
	  PYTHON_VERSION_FOR_EXTENSIONS=$$v dh_auto_build;		\
	done

# Similarly, I install everything, looping through all the available python
# versions, which could be more than just the default. All the non-python stuff
# will be installed multiple times, but it's the same files going to the same
# paths, so it hurts nothing
override_dh_auto_install:
	export VERSION=$(shell echo $(DEB_VERSION) | sed "s/-.*//");	\
	for v in `py3versions -s | sed s/python//g`; do			\
	  PYTHON_VERSION_FOR_EXTENSIONS=$$v dh_auto_install;		\
	done

override_dh_python3:
	dh_numpy3
	dh_python3

# don't compress docs
override_dh_compress:
	dh_compress -X.org -X.fig
